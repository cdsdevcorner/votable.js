/**
 * Classe de test du parseur VOTable.js
 *
 * @author Jérôme Desroziers
 */

 /*
 Fonctionnement de la librairie QUnit:
 L'arborescence est composée de modules, eux mêmes divisés en tests,
 qui contiennent des assertions.

 Structure type:
 QUnit.module('nom_module',{
  // Initialisation de variables
 });
 QUnit.test('nom_test', function(assert){
  // Code ...
  assert.FONCTION_DE_COMPARAISON(FONCTION_A_TESTER, RESULTAT_ATTENDU, 'description du resultat attendu');
  // On peut mettre d'autres asserts, etc...
 });

 Remarques: les tests ne sont pas contenus dans les chevrons du module,
 mais à la suite de ces derniers
 Concernant les fonctions de comparaison des asserts:
 strictEqual: égalité stricte
 deepEqual: égalité stricte récursive
 La profondeur de récursivité est définie via la variable QUnit.dump.maxDepth
 Pour tester le lever d'une exception: utilisation d'assert.throws
 assert.throws(function(){
      // fonction à tester, qui lève l'exception
  }, /exception/, 'description du résultat attendu');
 */

var p= new VOTableParser();
// maxDepth: permet d'effectuer des deepEqual avec la profondeur voulue
QUnit.dump.maxDepth = 9;

//¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤
QUnit.module('0_Tests sur le Chargement',{
});
QUnit.test('Le fichier existe et ne possède pas d\'extension', function(assert) {
	p.loadFile('test_files/0_Test_Chargement');
	assert.strictEqual(p.getFileLoadingState(), true, 'Fonction getFileLoadingState(): fichier chargé avec succès, tps de chargement: '+p.loadingTime+'ms');
});
QUnit.test('Le fichier existe et possède l\'extension .vot', function(assert) {
    	p.loadFile('test_files/0_Test_Chargement.vot');
    	assert.strictEqual(p.getFileLoadingState(), true, 'Fonction getFileLoadingState(): fichier chargé avec succès, tps de chargement: '+p.loadingTime+'ms');
});
QUnit.test('Le fichier existe et possède l\'extension .xml', function(assert) {
    	p.loadFile('test_files/0_Test_Chargement.xml');
    	assert.strictEqual(p.getFileLoadingState(), true, 'Fonction getFileLoadingState(): fichier chargé avec succès, tps de chargement: '+p.loadingTime+'ms');
});
QUnit.test('Le fichier n\'existe pas', function(assert) {
	p.loadFile('test_files/-1_Null_File');
	assert.strictEqual(p.getFileLoadingState(), false, 'Aucun fichier n\'a été chargé');
});
QUnit.test('Le fichier existe et est au format xml, mais n\'est pas au format VOTable', function(assert) {
	p.loadFile('test_files/0_Test_Chargement_Other_xml');
	assert.strictEqual(p.getFileLoadingState(), false, 'Fonction getFileLoadingState(): aucun fichier n\'a été chargé');
});
QUnit.test('Le fichier existe mais n\'est pas au format xml, ni même au format VOTable', function(assert) {
    	p.loadFile('test_files/0_Test_Chargement_Other');
      assert.strictEqual(p.getFileLoadingState(), false, 'Fonction getFileLoadingState(): aucun fichier n\'a été chargé');
});

//¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤
QUnit.module('I_Tests sur la Lecture',{
});
QUnit.test('Le fichier contient 0 ressources', function(assert) {
	p.loadFile('test_files/1_Test_Lecture_0R');
	assert.strictEqual(p.getNbTablesInFile(), 0, 'Fonction getNbTableVotable(): le fichier contient 0 tables');
	assert.strictEqual(p.getNbResourcesInFile(), 0, 'Fonction getNbResourcesInFile(): le fichier contient 0 ressources');
	p.selectResource(0);
  // dans le cas où l'on teste le déclenchement d'une exception, il faut utiliser assert.throws
  // ET placer le bloc lançant l'exception dans un bloc function(){ ... }
	assert.throws(function(){
    	p.selectTable(0);
	}, /selected.resource.tables is null/, 'Fonction selectTable(): sélectionner une table dans une ressource nulle lève l\'erreur \'selected.resource.tables is null\'');
	assert.strictEqual(p.getCurrentResourceNbTables(), 0, 'Fonction getCurrentResourceNbTables(): la ressource courante -nulle- contient 0 tables');
	assert.deepEqual(p.whoAmI(), { "resource": 0,
								   "table": 0,
								   "xml": "test_files/1_Test_Lecture_0R"
								 }, 'Fonction whoAmI(): est sélectionnée: ressource 0, table 0, de "test_files/1_Test_Lecture_1R1T"');
});
QUnit.test('Le fichier contient 1 ressource de 1 tables', function(assert) {
	p.loadFile('test_files/1_Test_Lecture_1R1T');
	assert.strictEqual(p.getNbTablesInFile(), 1, 'Fonction getNbTableVotable(): le fichier contient 1 table');
	assert.strictEqual(p.getNbResourcesInFile(), 1, 'Fonction getNbResourcesInFile(): le fichier contient 1 ressource');
	p.selectResource(0);
	p.selectTable(0);
	assert.strictEqual(p.getCurrentResourceNbTables(), 1, 'Fonction getCurrentResourceNbTables(): la ressource courante contient 1 table');
	assert.deepEqual(p.whoAmI(), { "resource": 0,
								   "table": 0,
								   "xml": "test_files/1_Test_Lecture_1R1T"
								 }, 'Fonction whoAmI(): est sélectionnée: ressource 0, table 0, de "test_files/1_Test_Lecture_1R1T"');
});
QUnit.test('Le fichier contient 1 ressource de 3 tables', function(assert) {
	p.loadFile('test_files/1_Test_Lecture_1R3T');
	assert.strictEqual(p.getNbTablesInFile(), 3, 'Fonction getNbTableVotable(): le fichier contient 3 table');
	assert.strictEqual(p.getNbResourcesInFile(), 1, 'Fonction getNbResourcesInFile(): le fichier contient 1 ressource');
	p.selectResource(0);
	p.selectTable(0);
	assert.strictEqual(p.getCurrentResourceNbTables(), 3, 'Fonction getCurrentResourceNbTables(): la ressource courante contient 3 tables');
	assert.deepEqual(p.whoAmI(), { "resource": 0,
								   "table": 0,
								   "xml": "test_files/1_Test_Lecture_1R3T"
								 }, 'Fonction whoAmI(): est sélectionnée: ressource 0, table 0, de "test_files/1_Test_Lecture_1R3T"');
});
QUnit.test('Le fichier contient 3 ressources de 3 tables', function(assert) {
	p.loadFile('test_files/1_Test_Lecture_3R3T');
	assert.strictEqual(p.getNbTablesInFile(), 9, 'Fonction getNbTableVotable(): le fichier contient 9 table');
	assert.strictEqual(p.getNbResourcesInFile(), 3, 'Fonction getNbResourcesInFile(): le fichier contient 3 ressources');
	p.selectResource(0);
	p.selectTable(0);
	assert.strictEqual(p.getCurrentResourceNbTables(), 3, 'Fonction getCurrentResourceNbTables(): la ressource courante contient 3 tables');
	assert.deepEqual(p.whoAmI(), { "resource": 0,
								   "table": 0,
								   "xml": "test_files/1_Test_Lecture_3R3T"
								 }, 'Fonction whoAmI(): est sélectionnée: ressource 0, table 0, de "test_files/1_Test_Lecture_3R3T"');
});

//¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤
QUnit.module('II_Tests sur le Format',{
});
QUnit.test('Le fichier est au format TABLEDATA', function(assert) {
	p.loadFile('test_files/2_Test_Encodage');
	p.selectResource(0);
    p.selectTable(0);
	assert.strictEqual(p.getCurrentTableEncoding(), 'UTF-8', 'Fonction getCurrentTableEncoding(): la 1ere table du fichier est au format TABLEDATA');
	assert.strictEqual(p.isCurrentTableEncodedInB64(), false, 'Fonction isCurrentTableEncodedInB64(): la 1ere table n\'est pas au format BINARY');
});
QUnit.test('Le fichier est au format BINARY', function(assert) {
	p.loadFile('test_files/2_Test_Encodage');
	p.selectResource(0);
    p.selectTable(1);
	assert.strictEqual(p.getCurrentTableEncoding(), 'BASE-64', 'Fonction getCurrentTableEncoding(): la 2eme table du fichier est au format BINARY');
	assert.strictEqual(p.isCurrentTableEncodedInB64(), true, 'Fonction isCurrentTableEncodedInB64(): la 2eme table du fichier est au format BINARY');
});

//¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤
QUnit.module('II_Tests sur le parsing des Metadata',{
});
QUnit.test('Le fichier ne possède pas de préfixe', function(assert) {
	p.loadFile('test_files/3_Test_GetMeta_Sans_Prefixe');
	assert.deepEqual(p.getVOTableMetadata(),   

{
  "coosyss": [],
  "groups": [],
  "infos": [
    {
      "ID": "status",
      "name": "QUERY_STATUS",
      "value": "OK"
    },
    {
      "ID": "ticket",
      "name": "Ticket",
      "value": "1467012866735"
    },
    {
      "ID": "reqTarget",
      "name": "-c",
      "value": "335.45025+5.010166666666668,rs=9164.103884177655"
    },
    {
      "ID": "reqEpoch",
      "name": "Epoch",
      "value": "2009-12-03T11:08:42"
    },
    {
      "ID": "supplier",
      "name": "Supplier",
      "value": "Provided by IMCCE/OBSPM/CNRS"
    }
  ],
  "params": [
    {
      "arraysize": "*",
      "datatype": "char",
      "name": "CooFrameOrigin",
      "utype": "stc:AstroCoordSystem.SpaceFrame.ReferencePosition",
      "value": "GEOCENTER"
    },
    {
      "arraysize": "*",
      "datatype": "char",
      "name": "Ephemeris",
      "utype": "stc:AstroCoordSystem.SpaceFrame.ReferencePosition.PlanetaryEphem",
      "value": "IMCCE-INPOP"
    }
  ],
  "version": "1.2",
  "xmlns": "http://www.ivoa.net/xml/VOTable/v1.2",
  "xmlns:xsi": "http://www.w3.org/2001/XMLSchema-instance",
  "xsi:schemaLocation": "http://www.ivoa.net/xml/VOTable/v1.2"
}, 'Les métadonnées du fichier VOTable prennent bien en compte les enfants direct de la balise, ainsi que les elts tels que les champs info situés après une ressource' );
		p.selectResource(0);
	assert.deepEqual(p.getCurrentResourceMetadata(), {
  "coosyss": [],
  "groups": [],
  "infos": [
    {
      "name": "QUERY_STATUS",
      "value": "OK"
    },
    {
      "name": "PROVIDER",
      "value": "TAPVizieR"
    },
    {
      "name": "QUERY"
    }
  ],
  "links": [],
  "params": [],
  "type": "results_I"
}, 'Même test, mais avec les métadonnées de la ressource courante');	
	p.selectResource(1);
	assert.deepEqual(p.getCurrentResourceMetadata(), {
  "coosyss": [],
  "groups": [],
  "infos": [
    {
      "name": "QUERY_STATUS",
      "value": "OK"
    },
    {
      "name": "PROVIDER",
      "value": "TAPVizieR"
    }
  ],
  "links": [],
  "params": [],
  "type": "results_II"
}, 'Même test, mais avec les métadonnées de la ressource courante, cette fois elle-même située dans une ressource');
p.selectTable(0);
	assert.deepEqual(p.getCurrentTableMetadata(), {
  "fields": [],
  "groups": [],
  "infos": [],
  "links": [],
  "params": [
    {
      "arraysize": "*",
      "datatype": "char",
      "name": "CooFrameOrigin",
      "utype": "stc:AstroCoordSystem.SpaceFrame.ReferencePosition",
      "value": "GEOCENTER"
    }
  ]
}, 'Même test, mais avec les métadonnées de la table courante');

});

QUnit.test('Le fichier possède un préfixe', function(assert) {
	p.loadFile('test_files/3_Test_GetMeta_Avec_Prefixe');
	assert.deepEqual(p.getVOTableMetadata(), {
  "coosyss": [],
  "groups": [],
  "infos": [
    {
      "ID": "status",
      "name": "QUERY_STATUS",
      "value": "OK"
    },
    {
      "ID": "ticket",
      "name": "Ticket",
      "value": "1467012866735"
    },
    {
      "ID": "reqTarget",
      "name": "-c",
      "value": "335.45025+5.010166666666668,rs=9164.103884177655"
    },
    {
      "ID": "reqEpoch",
      "name": "Epoch",
      "value": "2009-12-03T11:08:42"
    },
    {
      "ID": "supplier",
      "name": "Supplier",
      "value": "Provided by IMCCE/OBSPM/CNRS"
    }
  ],
  "params": [
    {
      "arraysize": "*",
      "datatype": "char",
      "name": "CooFrameOrigin",
      "utype": "stc:AstroCoordSystem.SpaceFrame.ReferencePosition",
      "value": "GEOCENTER"
    },
    {
      "arraysize": "*",
      "datatype": "char",
      "name": "Ephemeris",
      "utype": "stc:AstroCoordSystem.SpaceFrame.ReferencePosition.PlanetaryEphem",
      "value": "IMCCE-INPOP"
    }
  ],
  "version": "1.2",
  "xmlns:vot": "http://www.ivoa.net/xml/VOTable/v1.2",
  "xmlns:xsi": "http://www.w3.org/2001/XMLSchema-instance",
  "xsi:schemaLocation": "http://www.ivoa.net/xml/VOTable/v1.2"
}, 'Les métadonnées du fichier VOTable prennent bien en compte les enfants direct de la balise, ainsi que les elts tels que les champs info situés après une ressource' );
		p.selectResource(0);
	assert.deepEqual(p.getCurrentResourceMetadata(), {
  "coosyss": [],
  "groups": [],
  "infos": [
    {
      "name": "QUERY_STATUS",
      "value": "OK"
    },
    {
      "name": "PROVIDER",
      "value": "TAPVizieR"
    },
    {
      "name": "QUERY"
    }
  ],
  "links": [],
  "params": [],
  "type": "results"
}, 'Même test, mais avec les métadonnées de la ressource courante');	
	p.selectResource(1);
	assert.deepEqual(p.getCurrentResourceMetadata(), {
  "coosyss": [],
  "groups": [],
  "infos": [
    {
      "name": "QUERY_STATUS",
      "value": "OK"
    },
    {
      "name": "PROVIDER",
      "value": "TAPVizieR"
    }
  ],
  "links": [],
  "params": [],
  "type": "results"
}, 'Même test, mais avec les métadonnées de la ressource courante, cette fois elle-même située dans une ressource');
p.selectTable(0);
	assert.deepEqual(p.getCurrentTableMetadata(), {
  "fields": [],
  "groups": [],
  "infos": [],
  "links": [],
  "params": [
    {
      "arraysize": "*",
      "datatype": "char",
      "name": "CooFrameOrigin",
      "utype": "stc:AstroCoordSystem.SpaceFrame.ReferencePosition",
      "value": "GEOCENTER"
    }
  ]
}, 'Même test, mais avec les métadonnées de la table courante');
});

//¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤
QUnit.module('IV_Tests sur le parsing du Field et sur le traitement de fin de la chaîne en B64',{
});
QUnit.test('Le fichier possède 1 table de 1 colonne, de datatype short, et est codé en base 64', function(assert) {
	p.loadFile('test_files/4_Test_GetField');
	p.selectResource(0);
    p.selectTable(0);
	assert.deepEqual(p.getCurrentTableFields(), [{ "ID": "col1",
									  "datatype": "short",
    								  "name": "Short",
    								  "null": "-13"
  }], 'Fonction getCurrentTableFields(): on a bien récupéré l\'ensemble des champs de la colonne, ainsi que la valeur null contenue dans la balise <VALUES>');
	assert.strictEqual(p.getCurrentTableSpecificData(0,0), 42, 'La 1ere valeur de la 1ere ligne est 42');
	assert.strictEqual(p.getCurrentTableSpecificData(1,0), 24, 'La 1ere valeur de la 2eme ligne est 24');
	assert.strictEqual(p.getCurrentTableSpecificData(2,0), null, 'La 1ere valeur de la 3eme ligne n\'existe pas; les \'=\' en fin de chaîne ne sont pas interprétés comme des valeurs');
});

//¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤
QUnit.module('V_Tests sur le parsing du des Datas',{
});
QUnit.test('Tests sur les booléens, au formats TABLEDATA et BINARY', function(assert) {
	p.loadFile('test_files/5_Test_GetData_Boolean');
	p.selectResource(0);
    p.selectTable(0);
	assert.deepEqual(p.getCurrentTableData(), [
  [
    true,
    [
      true,
      false,
      false,
      false,
      false
    ]
  ],
  [
    null,
    []
  ]
], 'TABLEDATA, test sur valeur simple et tableau à 1 dimension, plus valeur nulle et tableau nul');
	p.selectTable(1);
	assert.deepEqual(p.getCurrentTableData(), [
  [
    true,
    [
      true,
      false,
      false,
      false,
      false
    ]
  ],
  [
    null,
    []
  ]
], 'BINARY, test sur valeur simple et tableau à 1 dimension, plus valeur nulle et tableau nul');
});
QUnit.test('Tests sur les bits, au formats TABLEDATA et BINARY', function(assert) {
	p.loadFile('test_files/5_Test_GetData_Bit');
	p.selectResource(0);
	p.selectTable(0);
	assert.deepEqual(p.getCurrentTableData(), [
  [
    0,
    42,
    [
      1,
      1,
      1,
      1,
      0
    ],
    5,
    [
      1,
      0,
      0,
      1,
      0,
      1,
      1,
      0,
      1,
      1,
      1,
      1,
      0,
      1,
      0,
      0,
      0,
      0,
      0,
      0,
      1,
      0,
      1,
      0,
      0,
      0,
      0
    ],
    [
      30,
      1
    ],
    [
      1,
      0,
      0,
      1,
      0,
      1,
      1,
      0,
      1,
      1,
      1,
      1,
      0,
      1,
      0,
      0,
      0,
      0,
      0,
      0,
      1,
      0,
      1,
      0,
      0,
      0,
      0
    ],
    2
  ]
], 'TABLEDATA, test sur valeurs bit simple, tableau de bits à taille fixe et à taille variable, avec elts séparateurs');
	p.selectTable(1);
	assert.deepEqual(p.getCurrentTableData(), [
  [
    0,
    42,
    [
      1,
      1,
      1,
      1,
      0
    ],
    5,
    [
      1,
      0,
      0,
      1,
      0,
      1,
      1,
      0,
      1,
      1,
      1,
      1,
      0,
      1,
      0,
      0,
      0,
      0,
      0,
      0,
      1,
      0,
      1,
      0,
      0,
      0,
      0
    ],
    [
      30,
      1
    ],
    [
      1,
      0,
      0,
      1,
      0,
      1,
      1,
      0,
      1,
      1,
      1,
      1,
      0,
      1,
      0,
      0,
      0,
      0,
      0,
      0,
      1,
      0,
      1,
      0,
      0,
      0,
      0
    ],
    2
  ]
], 'BINARY, test sur valeurs bit simple, tableau de bits à taille fixe et à taille variable, avec elts séparateurs');
});
QUnit.test('Tests sur les unsignedByte, au formats TABLEDATA et BINARY', function(assert) {
	p.loadFile('test_files/5_Test_GetData_UnsignedByte');
	p.selectResource(0);
	p.selectTable(0);
	assert.deepEqual(p.getCurrentTableData(), [
  [
    50,
    [
      10,
      20,
      30,
      99
    ]
  ],
  [
    null,
    []
  ]
], 'TABLEDATA, test sur valeurs unsignedByte simple, array, et valeur NULL');
	p.selectTable(1);
	assert.deepEqual(p.getCurrentTableData(), [
  [
    50,
    [
      10,
      20,
      30,
      99
    ]
  ],
  [
    null,
    []
  ]
], 'BINARY, test sur valeurs unsignedByte simple, array, et valeur NULL');
  p.cleanMemory();
});
QUnit.test('Tests sur les shorts, au formats TABLEDATA et BINARY', function(assert) {
	p.loadFile('test_files/5_Test_GetData_Short');
	p.selectResource(0);
	p.selectTable(0);
	assert.deepEqual(p.getCurrentTableData(), [
  [
    -7,
    [
      10,
      10,
      20,
      0,
      -1
    ]
  ],
  [
    null,
    []
  ]
], 'TABLEDATA, test sur valeurs short simple, array, et valeur NULL; et signe');
	p.selectTable(1);
	assert.deepEqual(p.getCurrentTableData(), [
  [
    -7,
    [
      10,
      10,
      20,
      0,
      -1
    ]
  ],
  [
    null,
    []
  ]
], 'BINARY, test sur valeurs short simple, array, et valeur NULL; et signe');
});
QUnit.test('Tests sur les ints, au formats TABLEDATA et BINARY', function(assert) {
	p.loadFile('test_files/5_Test_GetData_Int');
	p.selectResource(0);
	p.selectTable(0);
	assert.deepEqual(p.getCurrentTableData(), [
  [
    null,
    [
      -1,
      -2,
      3
    ]
  ],
  [
    null,
    []
  ]
], 'TABLEDATA, test sur valeurs ints simple, array, et valeur NULL; et signe');
	p.selectTable(1);
	assert.deepEqual(p.getCurrentTableData(), [
  [
    20,
    [
      -1,
      -2,
      3
    ]
  ],
  [
    42,
    []
  ]
], 'BINARY, test sur valeurs ints simple, array, et valeur NULL; et signe');
  p.cleanMemory();
});
QUnit.test('Tests sur les longs, au formats TABLEDATA et BINARY', function(assert) {
	p.loadFile('test_files/5_Test_GetData_Long');
	p.selectResource(0);
	p.selectTable(0);
	assert.deepEqual(p.getCurrentTableData(), [
  [
    -73022143065,
    [
      50,
      502,
      -500000000000,
      0
    ]
  ],
  [
    null,
    []
  ]
], 'TABLEDATA, test sur valeurs long simple, array, et valeur NULL; et signe');
	p.selectTable(1);
	assert.deepEqual(p.getCurrentTableData(), [
  [
    -73022143065,
    [
      50,
      502,
      -500000000000,
      0
    ]
  ],
  [
    null,
    []
  ]
], 'BINARY, test sur valeurs long simple, array, et valeur NULL; et signe');
	
});
QUnit.test('Tests sur les chars, au formats TABLEDATA et BINARY', function(assert) {
	p.loadFile('test_files/5_Test_GetData_Char');
	p.selectResource(0);
	p.selectTable(0);
	assert.deepEqual(p.getCurrentTableData(), [
  [
    "t",
    "RADEC 2000"
  ],
  [
    "",
    ""
  ]
], 'TABLEDATA, test sur valeurs chars simple et array');
	p.selectTable(1);
	assert.deepEqual(p.getCurrentTableData(), [
  [
    "t",
    "RADEC 2000"
  ],
  [
    "",
    ""
  ]
], 'BINARY, test sur valeurs chars simple et array');
	
});
QUnit.test('Tests sur les unicodeChars, au formats TABLEDATA et BINARY', function(assert) {
	p.loadFile('test_files/5_Test_GetData_UnicodeChar');
	p.selectResource(0);
	p.selectTable(0);
	assert.deepEqual(p.getCurrentTableData(), [
  [
    ""
  ]
], 'TABLEDATA, test sur valeurs unicodeChars simple et array');
	p.selectTable(1);
	assert.deepEqual(p.getCurrentTableData(), [
  [
    "䌀ሀ"
  ],
  [
    ""
  ]
], 'BINARY, test sur valeurs unicodeChars simple et array');
	
});
QUnit.test('Tests sur les floats, au formats TABLEDATA et BINARY', function(assert) {
	p.loadFile('test_files/5_Test_GetData_Float');
	p.selectResource(0);
	p.selectTable(0);
	assert.deepEqual(p.getCurrentTableData(), [
  [
    5.2,
    [
      1.2,
      0.0000023,
      0.5,
      26.48759,
      0
    ]
  ],
  [
    NaN,
    []
  ]
], 'TABLEDATA, test sur valeurs float simple, array, et valeur NULL; et signe');
	p.selectTable(1);
	assert.deepEqual(p.getCurrentTableData(), [
  [
    5.1999998,
    [
      1.2,
      0,
      0.5,
      26.4876,
      0
    ]
  ],
  [
    NaN,
    []
  ]
], 'BINARY, test sur valeurs float simple, array, et valeur NULL; et signe, et gestion de la précision');
	p.cleanMemory();
});
QUnit.test('Tests sur les doubles, au formats TABLEDATA et BINARY', function(assert) {
	p.loadFile('test_files/5_Test_GetData_Double');
	p.selectResource(0);
	p.selectTable(0);
	assert.deepEqual(p.getCurrentTableData(), [
  [
    5.2,
    [
      1.2,
      0.0000023,
      0.5,
      26.48759,
      0
    ]
  ],
  [
    NaN,
    []
  ]
], 'TABLEDATA, test sur valeurs double simple, array, et valeur NULL; et signe');
	p.selectTable(1);
	assert.deepEqual(p.getCurrentTableData(), [
  [
    5.2,
    [
      1.2,
      0.0000023,
      0.5,
      26.48759,
      0
    ]
  ],
  [
    NaN,
    []
  ]
], 'BINARY, test sur valeurs double simple, array, et valeur NULL; et signe');
});
QUnit.test('Test sur les float complexes, au formats TABLEDATA et BINARY', function(assert) {
	p.loadFile('test_files/5_Test_GetData_FloatComplex');
	p.selectResource(0);
	p.selectTable(0);
	assert.deepEqual(p.getCurrentTableData(), [
  [
    [
      5.2,
      2.8
    ],
    [
      [
        1.2,
        0.0000023
      ],
      [
        0.5,
        26.48759
      ],
      [
        0,
        2.5
      ]
    ]
  ],
  [
    [
      NaN,
      NaN
    ],
    []
  ]
], 'TABLEDATA, test sur valeurs float complexe simple, array (à plusieurs dimensions), et valeur NULL; et signe');
	p.selectTable(1);
	assert.deepEqual(p.getCurrentTableData(), [
  [
    [
      5.199999809265137,
      2.799999952316284
    ],
    [
      [
        1.2000000476837158,
        0.000002300000005561742
      ],
      [
        0.5,
        26.487590789794922
      ],
      [
        0,
        2.5
      ]
    ]
  ],
  [
    [
      NaN,
      NaN
    ],
    [// J_REMARQUE: en B64: tableau à en 2x3 c.f. TOPCAT qui parse TABLEDATA => BINARY
          // en remplaçant floatComplex par des array de float, et taille du tableau fixe,
          // i.e. [] pour arraySize de 6 donne [NaN, ...*6] ce qui est retransformé en tableau 2x3 de NaN par le parseur
      [
        NaN,
        NaN
      ],
      [
        NaN,
        NaN
      ],
      [
        NaN,
        NaN
      ]
    ]
  ]
], 'BINARY, test sur valeurs float complexe simple, array (à plusieurs dimensions), et valeur NULL; et signe');
	
});
QUnit.test('Test sur les doubles complexes, au formats TABLEDATA et BINARY', function(assert) {
	p.loadFile('test_files/5_Test_GetData_DoubleComplex');
	p.selectResource(0);
	p.selectTable(0);
	assert.deepEqual(p.getCurrentTableData(), [
  [
    [
      5.2,
      -Infinity
    ],
    [
      [
        1.2,
        Infinity
      ],
      [
        0.5,
        NaN
      ],
      [
        0,
        2.5
      ],
      [
        0,
        2.2
      ]
    ]
  ],
  [
    [
      NaN,
      NaN
    ],
    [
      [],
      [],
      [],
      []
    ]
  ]
], 'TABLEDATA, test sur valeurs double complexe simple, array (à plusieurs dimensions), et valeur NULL; et signe');
	p.selectTable(1);
	assert.deepEqual(p.getCurrentTableData(), [
  [
    [
      5.2,
      -Infinity
    ],
    [
      [
        1.2,
        Infinity
      ],
      [
        0.5,
        NaN
      ],
      [
        0,
        2.5
      ],
      [
        0,
        2.2
      ]
    ]
  ],
  [
    [
      NaN,
      NaN
    ],
    [// J_REMARQUE: même remarque que pour les float complexes
      [
        NaN,
        NaN
      ],
      [
        NaN,
        NaN
      ],
      [
        NaN,
        NaN
      ],
      [
        NaN,
        NaN
      ]
    ]
  ]
], 'BINARY, test sur valeurs double complexe simple, array (à plusieurs dimensions), et valeur NULL; et signe');
	p.selectResource(1);
  p.selectTable(0);
  assert.deepEqual(p.getCurrentTableData(), [
  [
    [
      255,
      32
    ],
    [
      [
        1.2,
        Infinity
      ],
      [
        0.5,
        NaN
      ],
      [
        0,
        2.5
      ],
      [
        0,
        2.2
      ]
    ]
  ]
], 'TABLEDATA, test sur valeurs double complexe simple, array (à plusieurs dimensions VARIABLE); et signe');
  p.selectTable(1);
  assert.deepEqual(p.getCurrentTableData(), [
  [
    [
      255,
      32
    ],
    [
      [
        1.2,
        Infinity
      ],
      [
        0.5,
        NaN
      ],
      [
        0,
        2.5
      ],
      [
        0,
        2.2
      ]
    ]
  ]
], 'BINARY, test sur valeurs double complexe simple, array (à plusieurs dimensions VARIABLE); et signe');
});
QUnit.test('Test principal sur le parsing des données', function(assert) {
	p.loadFile('test_files/5_Test_Main');
	p.selectResource(1);
	p.selectTable(0);
	assert.deepEqual(p.getCurrentTableData(), [
  [
    null,
    [
      25,
      42,
      57,
      2,
      2,
      2
    ],
    -73022143065,
    3.14159265359,
    [
      Infinity,
      NaN,
      3.14159265359,
      -0.000592,
      90111180000000
    ],
    3.14159265359,
    [
      Infinity,
      NaN,
      3.142,
      -0.001,
      9.011118e+103
    ],
    [
      [
        [
          1,
          1
        ],
        [
          0.1,
          0.1
        ],
        [
          2,
          2
        ],
        [
          0.2,
          0.2
        ]
      ],
      [
        [
          1.5,
          1.5
        ],
        [
          5.1,
          5.1
        ],
        [
          2.5,
          2.5
        ],
        [
          5.2,
          5.2
        ]
      ]
    ],
    [
      3.141593,
      1.618034
    ],
    [
      3.14,
      1.62
    ],
    [
      [
        [
          0.0005,
          NaN
        ],
        [
          -Infinity,
          600
        ]
      ]
    ],
    "AbCdEf",
    "zyx",
    "{!+}=(*)={+!}",
    "",
    [
      true
    ],
    42,
    [
      21,
      96,
      81,
      52
    ]
  ],
  [
    null,
    [
      -42
    ],
    -682,
    3.14159265359,
    [
      Infinity,
      NaN,
      3.14159265359,
      -0.000592,
      90111180000000
    ],
    null,
    [
      Infinity,
      NaN,
      3.142,
      -0.001,
      9.011118e+103
    ],
    [
      [
        [
          1,
          1
        ],
        [
          0.1,
          0.1
        ],
        [
          2,
          2
        ],
        [
          0.2,
          0.2
        ]
      ],
      [
        [
          1.5,
          1.5
        ],
        [
          5.1,
          5.1
        ],
        [
          2.5,
          2.5
        ],
        [
          5.2,
          5.2
        ]
      ]
    ],
    [
      3.141593,
      1.618034
    ],
    [
      3.14,
      1.62
    ],
    [
      [
        [
          0.0005,
          NaN
        ],
        [
          -Infinity,
          600
        ]
      ]
    ],
    "AbCdEf",
    "zyx",
    "{!+}=(*)={+!}",
    "",
    [
      true,
      false,
      false,
      true
    ],
    42,
    [
      21,
      96,
      81,
      52
    ]
  ]
], 'TABLEDATA, Main Test');
	p.selectTable(1);
	assert.deepEqual(p.getCurrentTableData(), [
  [
    null,
    [
      25,
      42,
      57,
      2,
      2,
      2
    ],
    -73022143065,
    3.1415927410125732,
    [
      Infinity,
      NaN,
      3.1415927410125732,
      -0.0005920000257901847,
      90111182110720
    ],
    3.14159265359,
    [
      Infinity,
      NaN,
      3.142,
      -0.001,
      9.011118e+103
    ],
    [
      [
        [
          1,
          1
        ],
        [
          0.1,
          0.1
        ],
        [
          2,
          2
        ],
        [
          0.2,
          0.2
        ]
      ],
      [
        [
          1.5,
          1.5
        ],
        [
          5.1,
          5.1
        ],
        [
          2.5,
          2.5
        ],
        [
          5.2,
          5.2
        ]
      ]
    ],
    [
      3.141593,
      1.618034
    ],
    [
      3.14,
      1.62
    ],
    [
      [
        [
          0.0005,
          NaN
        ],
        [
          -Infinity,
          600
        ]
      ]
    ],
    "AbCdEf",
    "zyx",
    "{!+}=(*)={+!}",
    "䌀ሀ",
    true,
    42,
    [
      21,
      96,
      81,
      52
    ]
  ],
  [
    null,
    42,
    -682,
    3.1415927410125732,
    [
      Infinity,
      NaN,
      3.1415927410125732,
      -0.0005920000257901847,
      90111182110720
    ],
    NaN,
    [
      Infinity,
      NaN,
      3.142,
      -0.001,
      9.011118e+103
    ],
    [
      [
        [
          1,
          1
        ],
        [
          0.1,
          0.1
        ],
        [
          2,
          2
        ],
        [
          0.2,
          0.2
        ]
      ],
      [
        [
          1.5,
          1.5
        ],
        [
          5.1,
          5.1
        ],
        [
          2.5,
          2.5
        ],
        [
          5.2,
          5.2
        ]
      ]
    ],
    [
      3.141593,
      1.618034
    ],
    [
      3.14,
      1.62
    ],
    [
      [
        [
          0.0005,
          NaN
        ],
        [
          -Infinity,
          600
        ]
      ]
    ],
    "AbCdEf",
    "zyx",
    "{!+}=(*)={+!}",
    "䌀ሀ",
    [
      true,
      false,
      false,
      true
    ],
    42,
    [
      21,
      96,
      81,
      52
    ]
  ]
], 'BINARY, Main Test');
	
});
//¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤
QUnit.module('VI_Tests sur le parsing des Groupes',{
});
QUnit.test('Le fichier ne possède pas de préfixe, et a des groupes définis au niveau du fichier VOTable, des ressources, et des tables, avec des groupes imbriqués, des FIELDrefs et des PARAMrefs', function(assert) {
  p.loadFile('test_files/6_Test_Groupes_Sans_Prefixe');
  assert.deepEqual(p.getVOTableGroups(),  

[
  {
    "ID": "1",
    "description": "groupe situé dans votable, contient d'autres groupes imbriqués",
    "groups": [
      {
        "ID": "1.1",
        "groups": [],
        "name": "VOTABLE_GROUPE1.1",
        "params": [
          {
            "name": "test_groupe_1.1"
          }
        ]
      },
      {
        "ID": "1.1",
        "groups": [
          {
            "ID": "1.1",
            "groups": [],
            "name": "VOTABLE_GROUPE1.2.1",
            "params": [
              {
                "ID": "PARAM_2",
                "arraysize": "*",
                "datatype": "char",
                "name": "Ephemeris",
                "utype": "stc:AstroCoordSystem.SpaceFrame.ReferencePosition.PlanetaryEphem",
                "value": "IMCCE-INPOP"
              }
            ]
          }
        ],
        "name": "VOTABLE_GROUPE1.2",
        "params": [
          {
            "name": "test_groupe_1.2"
          }
        ]
      }
    ],
    "name": "VOTABLE_GROUPE1",
    "params": [
      {
        "arraysize": "1",
        "datatype": "char",
        "name": "lower_limit",
        "ucd": "meta.code.error;stat.min",
        "value": "-"
      }
    ]
  }
], 'Fonction getVOTableGroups(): parsing des groupes du fichier VOTable');
  p.selectResource(0);
  assert.deepEqual(p.getCurrentResourceGroups(), [
    {
    "description": "groupe situé dans resource, contient paramref de param CooFrameOrigin",
    "groups": [],
    "id": "2",
    "name": "RESOURCE_GROUPE1",
    "params": [
      {
        "ID": "PARAM_1",
        "arraysize": "*",
        "datatype": "char",
        "name": "CooFrameOrigin",
        "utype": "stc:AstroCoordSystem.SpaceFrame.ReferencePosition",
        "value": "GEOCENTER"
      }
    ]
  }
], 'Fonction getCurrentResourceGroups(): parsing des groupes de la ressource no1');
  p.selectTable(0);
  assert.deepEqual(p.getCurrentTableGroups(), [
    {
    "description": "groupe situé dans une table, contient des fieldref ET des groupes imbriqués",
    "fields": [
      {
        "ID": "col1",
        "arraysize": "3*",
        "datatype": "short",
        "ucdRef": "ceciEstUnTest"
      }
    ],
    "groups": [
      {
        "fields": [
          {
            "ID": "col2",
            "datatype": "int"
          },
          {
            "ID": "col3",
            "datatype": "short"
          }
        ],
        "groups": [],
        "id": "3.1",
        "name": "TABLE_GROUPE1_ssgp",
        "params": []
      }
    ],
    "id": "3",
    "name": "TABLE_GROUPE1",
    "params": [
      {
        "ID": "PARAM_2",
        "arraysize": "*",
        "datatype": "char",
        "name": "Ephemeris",
        "ucdRef": "ceciEstUnTest_Deux",
        "utype": "stc:AstroCoordSystem.SpaceFrame.ReferencePosition.PlanetaryEphem",
        "value": "IMCCE-INPOP"
      }
    ]
  }
], 'Fonction getCurrentTableGroups(): parsing des groupes de la table no1 de la ressource no1');
});
QUnit.test('Le fichier possède un préfixe, et a des groupes définis au niveau du fichier VOTable, des ressources, et des tables, avec des groupes imbriqués, des FIELDrefs et des PARAMrefs', function(assert) {
  p.loadFile('test_files/6_Test_Groupes_Avec_Prefixe');
  assert.deepEqual(p.getVOTableGroups(), [
    {
    "description": "groupe situé dans votable, contient d'autres groupes imbriqués",
    "groups": [
      {
        "groups": [],
        "id": "1.1",
        "name": "VOTABLE_GROUPE1.1",
        "params": [
          {
            "name": "test_groupe_1.1"
          }
        ]
      },
      {
        "groups": [
          {
            "groups": [],
            "id": "1.1",
            "name": "VOTABLE_GROUPE1.2.1",
            "params": [
              {
                "ID": "PARAM_2",
                "arraysize": "*",
                "datatype": "char",
                "name": "Ephemeris",
                "utype": "stc:AstroCoordSystem.SpaceFrame.ReferencePosition.PlanetaryEphem",
                "value": "IMCCE-INPOP"
              }
            ]
          }
        ],
        "id": "1.1",
        "name": "VOTABLE_GROUPE1.2",
        "params": [
          {
            "name": "test_groupe_1.2"
          }
        ]
      }
    ],
    "id": "1",
    "name": "VOTABLE_GROUPE1",
    "params": [
      {
        "arraysize": "1",
        "datatype": "char",
        "name": "lower_limit",
        "ucd": "meta.code.error;stat.min",
        "value": "-"
      }
    ]
  }
], 'Fonction getVOTableGroups(): parsing des groupes du fichier VOTable');
  p.selectResource(0);
  assert.deepEqual(p.getCurrentResourceGroups(), [
    {
    "description": "groupe situé dans resource, contient paramref de param CooFrameOrigin",
    "groups": [],
    "id": "2",
    "name": "RESOURCE_GROUPE1",
    "params": [
      {
        "ID": "PARAM_1",
        "arraysize": "*",
        "datatype": "char",
        "name": "CooFrameOrigin",
        "utype": "stc:AstroCoordSystem.SpaceFrame.ReferencePosition",
        "value": "GEOCENTER"
      }
    ]
  }
], 'Fonction getCurrentResourceGroups(): parsing des groupes de la ressource no1');
  p.selectTable(0);
  assert.deepEqual(p.getCurrentTableGroups(), [
  {
    "description": "groupe situé dans une table, contient des fieldref ET des groupes imbriqués",
    "fields": [
      {
        "ID": "col1",
        "arraysize": "3*",
        "datatype": "short"
      }
    ],
    "groups": [
      {
        "fields": [
          {
            "ID": "col2",
            "datatype": "int"
          },
          {
            "ID": "col3",
            "datatype": "short"
          }
        ],
        "groups": [],
        "id": "3.1",
        "name": "TABLE_GROUPE1_ssgp",
        "params": []
      }
    ],
    "id": "3",
    "name": "TABLE_GROUPE1",
    "params": [
      {
        "ID": "PARAM_2",
        "arraysize": "*",
        "datatype": "char",
        "name": "Ephemeris",
        "utype": "stc:AstroCoordSystem.SpaceFrame.ReferencePosition.PlanetaryEphem",
        "value": "IMCCE-INPOP"
      }
    ]
  }
], 'Fonction getCurrentTableGroups(): parsing des groupes de la table no1 de la ressource no1');
});

//¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤
QUnit.module('VII_Tests sur les fonctions de rendu html',{
});
QUnit.test('Tests sur les booléens, au formats TABLEDATA et BINARY', function(assert) {
  p.loadFile('test_files/7_Test_Html');
  p.selectResource(1);
  p.selectTable(0);
  assert.equal(p.getHtmlCurrentTableFields(),
   '<tr><th>Int</th><th>Short</th></tr>',
   'Fonction getHtmlCurrentTableFields(), rendu des fields au format html');
  assert.equal(p.getHtmlCurrentTableData(),
   '<tr><td>null</td><td>-10,0,42</td></tr><tr><td>null</td><td>5,5,10,35,-127</td></tr>',
   'Fonction getHtmlCurrentTableData(), rendu des données au format html');
});