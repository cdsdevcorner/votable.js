var fileNames=['vizier_votable.b64',
			   'light64.xml',
			   'light.xml',
			   'medium64.xml',
			   'medium.xml',
			   'resource.xml',
			   'big64.xml',
			   'big.xml',
			   'vizier_votable4.vot',
			   'resource2.xml',
			   'vizier_votable6.vot',
			   'fat_lv-1.b64',
			   'fat_lv0.b64',
			   'fat_lv0.vot',
			   'fat_lv1.b64',
			   'fat_lv1.vot',
			   'fat_lv2.b64',
			   'fat_lv2.vot'];

var mode=0;
var runTestOnTabledata300mo=false;

var res='';

var p_upToDate= new VOTableParser();
var p_depreciated;

res+="<div class=\"fileName\"><p>TESTS DE PERFORMANCE</p></div>";
res+="<div class=\"res_upToDate\"><p>V_UPTODATE</p></div>";
res+="<div class=\"res_depreciated\"><p>V_DEPRECIATED</p></div>";

var total_upToDate_load=0, total_upToDate_parsing=0,
    total_depreciated_load=0, total_depreciated_parsing=0,
    total_depreciated, time,
    loaded_upToDate=0, parsed_upToDate=0,
    loaded_depreciated=0, parsed_depreciated=0
    nbFat=0;

document.write(res);

for(var i=0; i<fileNames.length; i++){

	res='';
	var isLightData= fileNames[i].indexOf('fat') === -1;

	if(!isLightData)
		nbFat+=1;

	if(isLightData || runTestOnTabledata300mo){

	res+="<div class=\"fileName\"><p>Chargement de "+fileNames[i]+"</p></div>";

	//---------- upToDate
	
	p_upToDate.loadFile('test_files/'+fileNames[i]);

	if(p_upToDate.getFileLoadingState()){

	loaded_upToDate+=1;

	time=p_upToDate.loadingTime/1000;
	total_upToDate_load+=time;
	res+="<div class=\"res_upToDate\"><p>Tps de chargement: "+time+"s</p>";

	p_upToDate.selectResource(0);

	p_upToDate.selectTable(0);

	p_upToDate.getCurrentTableData();

	time=p_upToDate.parsingTime/1000;
	total_upToDate_parsing+=time;
	res+="<p>Tps de parsing: "+time+"s</p></div>";

	}else{
		res+="<div class=\"res_upToDate\"><p>NaN</p>";
		res+="<p>NaN</p></div>";
	}

    //---------- depreciated

    if(!runTestOnTabledata300mo){

    p_depreciated= new Parser();

	p_depreciated.load('test_files/'+fileNames[i]);

	if(p_depreciated.IsLoad()){

	loaded_depreciated+=1;

	time=p_depreciated.loadingTime/1000;
	total_depreciated_load+=time;
	res+="<div class=\"res_depreciated\"><p>Tps de chargement: "+time+"s</p>";

	p_depreciated.SelectResource(0);

	p_depreciated.SelectTable(0);

	p_depreciated.GetData();

	time=p_depreciated.parsingTime/1000;
	total_depreciated_parsing+=time;
	res+="<p>Tps de parsing: "+time+"s</p></div>";

	}else{
		res+="<div class=\"res_depreciated\"><p>NaN</p>";
		res+="<p>NaN</p></div>";
	}

	}

	document.write(res);
    
    }

}

res='';

res+="<div class=\"fileName\"><p>BILAN</p></div>";

res+="<div class=\"res_upToDate\"><p>Fichiers chargés avec succès: "+loaded_upToDate+"/"+(fileNames.length-nbFat)+"</p></div>";

res+="<div class=\"res_depreciated\"><p>Fichiers chargés avec succès: "+loaded_depreciated+"/"+(fileNames.length-nbFat)+"</p></div>";

document.write(res);