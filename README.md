# votable.js

JavaScript parser for VOTable

## Description

`votable.js` is a JavaScript parser designed to handle the VOTable
format.  The VOTable format is used in many astronomical services and
tools.  VOTable is a recommendation from the IVOA (International
Virtual Observatory Alliance).  The VOTable specification is here:
[http://www.ivoa.net/documents/VOTable/](http://www.ivoa.net/documents/VOTable/)
The last version of VOTable is 1.3

## History

This work was started in September 2014 and deeply updated in the mid-2016.

## Capabilities

What the parser CAN do:

* parse VOTable files, whether they possess a prefix or not.
* parse data either in `TABLEDATA` or in `BINARY` format.
* parse the whole field of values given by the VOTable documentation
  (boolean, bit, unsignedByte, short, int, long, char, unicodeChar,
  float, double, floatComplex, doubleComplex).
* take into account the null value (represented by a magical number).
* take into account the precision for the float, double, floatComplex
  and doubleComplex types.
* parse arrays (with one or more dimensions, with fixed length or not).
* parse groups.
* takes in account the `PARAMrefs` and `FIELDrefs` fields, and
  replaces them by their real equivalent when parsing the metadata.

What the parser CAN'T do:

* parse multidimensional arrays of chars or unicodeChars.
* link `PARAMrefs` located in a sub-group that references a `PARAM`
  not located in the VOTable / resource / table parent but in a parent
  `GROUP`. Example:

        <RESOURCE>
          <PARAM ID="1"/>
          <GROUP>
              <PARAM ID="2"/>
              <GROUP>
                  <PARAMref ref="2"/>
              </GROUP>
          </GROUP>
        </RESOURCE>

  The `ref` attribute of the `PARAMref` element can only be a
  reference to a param defined as a direct child of the VOTable /
  resource / table parents elements.

* associate a null value to a specific element of an array (i.e. for
  an array, either the whole array is null and is equal to the null
  value, or none of its values is null).  About the specific cases,
  further explanation is given within the code.

## Usage

I. Creation of an instance of the parser

    * `var p = new VOTableParser();`

    * It is also possible to use the parser as a `CommonJS` module:
      `var p = require('votable.js');`

    Additionaly, you can disable/enable the displaying of debug infos
    in the console, and add callback functions that will trigger at
    the end of each file's loading:

    * `p.displayErrors(boolean);`
    * `p.setCallbackFunctions(callback, errCallback);`

II. Load a VOTable file:

    * `p.loadFile(pathOrURL);`

    If the given path/url is correct and the VOTable file is readable,
    `callback` will be called at the end of the loading. Otherwise
    `errCallback` is called.

    * `p.loadBufferedFile(buffer, isXml);` This is used when you
    already possess the VOTable file in the form of a string/xml
    buffer.  The value of `isXml`'s value should be set to `true` if
    the buffer is an xml tree (and to `false` if it's a string).

    * `p.getFileLoadingState();`
    This can be used to know if the file has been successfully loaded
    (the function returns a boolean).

III. Get general information about the loaded file
    
The following functions will give you informations about the total number of tables/resources contained ithin the file:
    * `p.getNbResourcesInFile();`
    * `p.getNbTablesInFile();`

    The following functions will give you informations about the
    VOTable file's metadata:

    * `p.getVotableMetadata();`

    It will return a JSON object containing the VOTable xml fields and
    their attributes, but will not contain following xml fields:
    resource, table, field, data.  Example:

        {
          "coosyss": [],
          "groups": [], // (*)
          "infos": [ { "ID": "status", "name": "QUERY_STATUS", "value": "OK" },
                     { "ID": "supplier", "name": "Supplier", "value": "Provided by IMCCE/OBSPM/CNRS" }
                   ],
          "params": [
            { "arraysize": "*",
              "datatype": "char",
              "name": "CooFrameOrigin",
              "utype": "stc:AstroCoordSystem.SpaceFrame.ReferencePosition",
              "value": "GEOCENTER"
            }
          ],
          "version": "1.2",
          "xmlns": "http://www.ivoa.net/xml/VOTable/v1.2",
          "xmlns:xsi": "http://www.w3.org/2001/XMLSchema-instance",
          "xsi:schemaLocation": "http://www.ivoa.net/xml/VOTable/v1.2"
        }

    Note that the arrays containing xml fields such as `PARAM`,
    `FIELD`, etc... are named as metadata's attributes with their xml
    fieldname, in lower case with an 's' char at the end.  Also, they
    are defined as empty arrays if the metadata doesn't contain them
    (see in the example above the `groups` marked (*) ).  The
    deprecated xml field `COOSYS` is recognized by the parser, so an
    empty array will be created if it doesn't exist in the metadata,
    and it will be named `coosys`.

    * `p.getVotableGroups();`

    This is more specific than the previous method; rather than
    returning all the metadata it will simply return the array of the
    metadata's groups (each group will be represented as a JSON
    object).

    * `p.whoAmI();`

    This returns a JSON object whose structure is:

        { 'xml': filePath, 'resource': idCurrentResource, 'table': idCurrentTable }

IV. Select a resource and get the information it contains
    
First you must select the resource that interrests you by using the following method:

    * `p.selectResource(id);`

    `id` must be a number beetween 0 and the file's total number of
    resources minus 1.

    * `p.getCurrentResourceNbTables();`

    This returns the number of tables in the current resource.

    As their equivalents in section 3, the following functions will give you
    informations about the current resource's metadata:

    * `p.getCurrentResourceMetadata();`
    * `p.getCurrentResourceGroups();`

V. Select a table in the current resource and get the information it contains
    
First you must select the table that interrests you by using the following method:

    * `p.selectTable(id);`

    Note that the table must be contained in the currently selected
    resource, and `id` must be beetween 0 and the current's resource
    number of tables minus 1.

    Theses two functions give information about the current table encoding:

    * `p.getCurrentTableEncoding();` will return a string ('UTF-8' or
      'BASE-64').
    * `p.isCurrentTableEncodedInB64();` will return a boolean.

    The following functions will parse the data contained within the
    current table:

    * `p.getCurrentTableFields();`

    This will return an array of JSON objects, one for each field of
    the current table (with its attributes, such as precision,
    etc...).  Example:

        [
          {
            "ID": "col1",
            "datatype": "unsignedByte",
            "name": "UnsignedByte"
          },
          {
            "ID": "col2",
            "arraysize": "*",
            "datatype": "unsignedByte",
            "name": "UnsignedByte_Array_Variable"
          }
        ]

    * `p.getCurrentTableData();`

    This will return a two-dimensional array, the first dimension will
    represent the lines and the second the columns.  Example:

        [
          [
            -7,
            [
              10,
              10,
              20,
              0,
              -1
            ]
          ]
        ]

    This is the result obtained when using the function on the table
    whose fields have been examined previously.

    * `p.getCurrentTableSpecificData(i, j);`

    This will return the specific value located at the line `i` and
    the column `j`.

    As their equivalents in section 3, the following functions will give you
    informations about the current table's metadata:

    * `p.getCurrentTableMetadata();`
    * `p.getCurrentTableGroups();`

    Following functions will each return a string containing the html
    representation of the results returned respectively by
    `getCurrentTableFields()` and `getCurrentTableData()`.

    * `p.getHtmlCurrentTableFields();`
    * `p.getHtmlCurrentTableData();`

VI. Finally, you can reset all the data stored inside the parser by using the function:

    * `p.cleanMemory();`

    This is usefull before loading a new file, using the same parser
    object.  It also allows to reclaim memory if the parser is no more
    used.

## Example

        var p = new VOTableParser();
        p.loadFile('test.vot');

        var nbResources = p.getNbResourcesInFile();
        var nbTablesInResource = 0;
        var currentTableGroups = [];
        var currentTableFields = [];
        var currentTableData = [[]];

        for(var i = 0; i < nbResources; i++) {
            p.selectResource(i);
            nbTablesInResource = p.getCurrentResourceNbTables();
            for(var j = 0; j < p.getCurrentResourceNbTables; j++) {
                p.selectTable(j);
                currentTableGroups = p.getCurrentTableGroups();
                currentTableFields = p.getCurrentTableFields();
                currentTableData = p.getCurrentTableData();
                // ... do something
            }
        }
        // ... do something again
        p.cleanMemory();

More specific examples can be found in the `test.js` file.

## Authors

Thomas Rolling, J&eacute;r&ocirc;me Desroziers (Core source code)

Contributors (code update, documentation, tests, etc.):
Thomas Boch, Gilles Landais, André Schaaff, Anne-Camille Simon, Pascal Wassong

For any question: André Schaaff
